package com.ruoyi.project.common;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 爬取电影
 * @author qxw
 * @version 1.00
 * @time  13/6/2019 下午 5:35
 */
@Component
public class MovieApi {
    private  final Logger logger= LoggerFactory.getLogger(this.getClass());

    /**
     * 最快资源网电影分类  爬取链接
     */
    private static  final String  ZUIKZY_CC_MOVIE="http://zuikzy.cc/?m=vod-type-id-TYPE-pg-PAGE.html";

    public static void main(String[] args) throws Exception {

        MovieApi api=new MovieApi();
        for (int i = 1; i <=5 ; i++) {
            api.movie(1,i);
            Thread.sleep(1000);
        }

    }

    /**
     *
     * 爬取最快资源网影视资源
     * @param sourceType 类型 1=电影 2=连续剧 3=综艺 4=动漫
     * @param page 页码
     */
    public void  movie(int sourceType,int page){
        try{
            logger.info("【开始爬取最快资源网...】={},page={}",sourceType,page);
            Document doc = Jsoup.connect(ZUIKZY_CC_MOVIE.replaceAll("TYPE",String.valueOf(sourceType)).replaceAll("PAGE",String.valueOf(page))).userAgent("Mozilla").timeout(50000).get();
            //读取数据列表对象
            Element element = doc.getElementById("data_list");
            Elements list=  element.getElementsByTag("tr");
            for(Element e:list){
//              System.out.println(e.text());
                Elements tdList=e.getElementsByTag("td");
                String name=tdList.get(0).text();
                String detailsUrl="http://zuikzy.cc"+tdList.get(0).getElementsByTag("a").get(0).attr("href");
                String type=tdList.get(1).text();
                String country=tdList.get(2).text();
                String state=tdList.get(3).text();

                //获取详情页图片 简介和播放地址
                Document detailsDoc = Jsoup.connect(detailsUrl).userAgent("Mozilla").timeout(50000).get();

                Element imgDoc=detailsDoc.getElementsByClass("videoPic").get(0);

                String imgUrl=imgDoc.getElementsByTag("img").get(0).attr("src");

                System.out.println(name+"  "+type+"  "+"  "+country+"  "+state+"  "+detailsUrl);

            }
        }catch (Exception e){
            logger.error("【爬取最快资源网异常....】={}",e);
        }
    }

}
